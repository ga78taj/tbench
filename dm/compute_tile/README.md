# Compute Tile System

#### with new FuseSoC package manager


Before starting with this introduction make sure you have worked through the [OpTiMSoc User Guide](http://www.optimsoc.org/docs/user-guide/chap_tutorials.html). Chapters "Introduction" and "Installation & Configuration" are essential. Afterwords you can continue here.

This is a modified version of OpTiMSoC so you'll have to replace some files.
Delete the directories

    ~/optimsoc/src/rtl
    ~/optimsoc/tbench
    
and replace them with 

    https://gitlab.lrz.de/ga78taj/rtl.git
    https://gitlab.lrz.de/ga78taj/tbench.git
    
the new part in this OpTiMSoC version is, we are using FuseSoC.

FuseSoC is an package manager and a set of build tools for HDL code, reusing IP cores.
We are using a mor1kx core from [openrisc open-cores](https://github.com/openrisc/orpsoc-cores).
At the moment only [verilator](http://www.veripool.org/wiki/verilator) is supported as simulator.

Make sure you read thorugh the Introduction and install the software. [github.com/olofk/fusesoc](https://github.com/olofk/fusesoc) 

To understand the basics we will build a single core processor in one compute tile without any Network-on-Chip. 
    
We navigate through our file system to the directory

    ~/optimsoc/tbench/dm/compute_tile/
    
Next we use fusesoc to generate the simulation file

    fusesoc --verbose --cores-root "." --cores-root "../../../src/rtl" --cores-root "../../../src/" sim compute_tile --elf-load ct.vmem

- we start with the command fusesoc, our package manager
- add verbose, so we see what's going on in details
- cores-root adds paths fusesoc will look for source files (fusesoc passes them to verilator)
- compute_tile is the core we want to simulate
- elf-load expects the memory file (ct.vmem) of the programm we want to simulate 

 the important key word we want to look at is "compute_tile", it
 - is the core file we are going to start with
 - holds information about the simulation (verilator)
 - loads more modules: compute_tile_dm and trace_monitor.r3_checker
 - sets tb_compute_tile as top_module
  
compute_tile_dm gets loaded by compute tile, it
- adds a lot of modules / .core files: mor1kx_module, networkadapter_ct, bootrom, sram.plain, debug_system.mam_wb_adapter, wb_interconnect.bus
- mor1kx gets mor1kx.capuccino
- network_ct gets lisnoc.dma and lisnoc.mp_simple

each one of the cores inkludes at least one verilog file which is required by the verilator. For the details please have a look at the .core files, they are located at: 

    ~/optimsoc/src/rtl/ 
    
The .v files are copied to path: 
    
    ~/optimsoc/tbench/dm/compute_tile/build/compute_tile/src/

Now all files needed for the simulation are ready to go and verilator can do it's work. We see the output:

    Info: Verilating source
    Info: Starting verilator
    
Verilator converts synthesizeable Verilog code, ... into C++ or SystemC code. It is not a complete simulator but a Compiler.

The next line of output the verilator source path and a lot of CFlags & LDFlags are displayed ... TBD The flags pointing on target path (sim-verilator), source path (/src), tb_compute_tile.cpp and different include files.

Next informations showing up are:

    INFO:  Building verilator executable:
    INFO:    Verilator executable working dir: /home/ga78taj/src/optimsoc/tbench/dm/compute_tile/build/compute_tile/sim-verilator/obj_dir
    INFO:    Verilator executable command: make -f Vtb_compute_tile.mk Vtb_compute_tile
    INFO:  /home/ga78taj/src/optimsoc/tbench/dm/compute_tile/build/compute_tile/sim-verilator/obj_dir
    INFO:      make -f Vtb_compute_tile.mk Vtb_compute_tile

Progress to build the executable file (Vtb_compute_tile) using the Makefile Vtb_compute_tile.mk.
As the executable is built the simulation process starts:

    INFO:  Running simulation
    INFO:  /home/ga78taj/src/optimsoc/tbench/dm/compute_tile/build/compute_tile/sim-verilator/obj_dir
    INFO:      ./Vtb_compute_tile --elf-load ct.vmem

The new build directory ~/compute_tile/build/compute_tile/sim-verilator/obj-dir/ doesn't hold a ct.vmem file, the simulation aborts with Error Message:

    %Error: ct.vmem:0: $readmem file not found
    Aborting...
    ERROR: Failed to run the simulation
    ERROR: "./Vtb_compute_tile --elf-load ct.vmem" exited with an error code.
    ERROR: See stderr for details.
    
We have to provide the .vmem file manually.
    
### Generate .vmem file an link to our directory
Build an application from optimsoc/baremetal-apps, e.g.: hello

    make -C ${OPTIMSOC_APPS}/baremetal/hello
This will generate .o, .elf and .vmem files, the last needed for simulation

Make sure you are in 

    ~/optimsoc/tbench/dm/compute_tile/

Then you need to link to the memory initialization file (`ct.vmem`)

    ln -s ${OPTIMSOC_APPS}/baremetal/hello/hello.vmem ct.vmem
    
or you can move the file manually.

Start the simulation 

    build/compute_tile/sim-verilator/obj_dir/Vtb_compute_tile standalone
    
standalone keeps the simulation from connect to optimsoc_cli, which would fail

two files are generated

    events.000      lists all occured events during simulation
    stdout.000      lists the output generated by the programm (e.g. printf)

if you simulated hello the stdout will be


    Hello world!
    
    




